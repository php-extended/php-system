<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-system library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\System\Os2;
use PHPUnit\Framework\TestCase;

/**
 * Os2Test test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\System\Os2
 *
 * @internal
 *
 * @small
 */
class Os2Test extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Os2
	 */
	protected Os2 $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Os2();
	}
	
}
