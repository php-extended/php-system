<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-system library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\System;

/**
 * Darwin class file.
 *
 * This class represents an operating system of the Darwin family.
 *
 * @author Anastaszor
 * @see https://en.wikipedia.org/wiki/Darwin_(operating_system)
 */
class Darwin extends OperatingSystem
{
	
	/**
	 * {@inheritDoc}
	 *
	 * @see OperatingSystem::isUnix()
	 */
	public function isUnix() : bool
	{
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 *
	 * @see OperatingSystem::isWindows()
	 */
	public function isWindows() : bool
	{
		return false;
	}
	
}
