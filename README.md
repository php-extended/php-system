# php-extended/php-system
A generic way to find informations about your system

![coverage](https://gitlab.com/php-extended/php-system/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-system/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-system ^8`


## Basic Usage

You can get the singleton php-system object by doing the following:

```php

use PhpExtended\System\OperatingSystem;

$system = OperatingSystem::get();

```

This system object have lots of useful properties. An instance of `UnknownOs` is returned in case this is not implemented.
The `get` method never throws any exception.


## License

MIT (See [license file](LICENSE)).
